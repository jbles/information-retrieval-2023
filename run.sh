#!/bin/bash -e
#SBATCH --partition=csedu
#SBATCH --account=csedui00041
#SBATCH --gres=gpu:1
#SBATCH --mem=15G
#SBATCH --cpus-per-task=1
#SBATCH --time=10:00:00
#SBATCH --output=my-experiment-%j.out
#SBATCH --error=my-experiment-%j.err
source /scratch/$USER/virtual_environments/information-retrieval-2023/bin/activate

python fine-tune_monoT5.py

python rank.py

python rank.py pretrained