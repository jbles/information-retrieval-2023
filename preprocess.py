from global_var_lib import *
from rdflib import Graph, Literal
import json
import os
import random
from split_dataset import *

os.makedirs(preprocess_directory, exist_ok=True)
os.makedirs(parse_directory, exist_ok=True)

# Split DBpedia collection into about 20 sub files so processing with rdflib actually works.

if not test_small_batch:
  file = open(input_file, 'r', encoding='utf-8')
  file_lines = file.readlines()
  file.close()
  file_size = len(file_lines)
  part_size = file_size // split_file_count

  for i in range(split_file_count):
      start = i * part_size
      if i + 1 == split_file_count:
          end = file_size - 1
      else:
          end = (i + 1) * part_size
      part_content = file_lines[start:end]

      output_file = f'{preprocess_directory}/part{i+1}.ttl'
      with open(output_file, 'w', encoding='utf-8') as output_file:
          output_file.write('\n'.join(part_content))



# Parse all split files with rdflib and save them in one big folder with all documents
def parse_collection_data(input_file, output_directory, file_index, doc_lookup):
  # Load your RDF data (replace 'your_rdf_data.ttl' with the path to your RDF file)
  g = Graph()
  g.parse(input_file, format="turtle")

  # Define the directory to store plain text documents
  os.makedirs(output_directory, exist_ok=True)

  # Define a namespace for RDF predicates
  RDFS = "http://www.w3.org/2000/01/rdf-schema#"

  max_docs_per_file = 200000
  i = 0

  # Iterate through RDF triples and extract text content
  for i, (subj, pred, obj) in enumerate(g):
      if str(pred) == RDFS + "comment":
          # 'subj' contains the resource URI, and 'obj' contains the text content
          resource_uri = "<dbpedia:" + str(subj).split('/')[-1] + ">"
          text_content = str(obj)

          if i % max_docs_per_file == 0:
              if i > 0:
                  output_jsonl_file.close()
              output_filename = 'docs{:02d}.json'.format(file_index)
              output_path = os.path.join(output_directory, output_filename)
              output_jsonl_file = open(output_path, 'w', encoding='utf-8', newline='\n')
              file_index += 1
          output_dict = {'id': resource_uri, 'contents': text_content}
          doc_lookup[resource_uri] = {'lnr': (i % max_docs_per_file) + 1, 'filename': output_filename}
          output_jsonl_file.write(json.dumps(output_dict) + '\n')

          if i % 100000 == 0:
              print(f'Converted {file_index:,} docs, writing into file {file_index}')

  output_jsonl_file.close()
  file_index += 1
  return file_index, doc_lookup


file_index = 0
doc_lookup = {}
# Loop through all files and parse files
for filename in os.listdir(preprocess_directory):
    file_path = os.path.join(preprocess_directory, filename)
    if os.path.isfile(file_path) and (filename.endswith('.txt') or filename.endswith('.ttl')):
      print(file_path)
      print(parse_directory)
      file_index, doc_lookup = parse_collection_data(file_path, parse_directory, file_index, doc_lookup)



# The query string for each topicid is querystring[topicid]
querystring = {}
with open(queries_file, 'r') as file:
    for line in file:
        query_id, query_text = line.strip().split('\t')
        querystring[query_id] = query_text

def write_line(topicid, positive_passage, negative_passage):
  text_topic_id = querystring[topicid]
  text_positive_passage = get_content_from_docsjson(positive_passage)
  text_negative_passage = get_content_from_docsjson(negative_passage)
  if text_positive_passage and text_negative_passage:
    output_file.write(text_topic_id + "\t" + text_positive_passage + "\t" + text_negative_passage + "\n")

def get_content_from_docsjson(doc_id):
  if doc_id not in doc_lookup:
    return None
  content_information = doc_lookup[doc_id]

  with open(os.path.join(parse_directory, content_information['filename']), "r", encoding='utf-8') as doc_file:
    doc_content = doc_file.readlines()
    json_text = doc_content[content_information['lnr'] - 1]
    json_doc = json.loads(json_text)

  return json_doc['contents']

queries_train, queries_test, y_train, y_test = split_dataset()
# For each topicid, the list of positive docids is qrel[topicid]
with open(qrels_file, "r", encoding='utf-8') as input_file, \
    open(triples_file, "w", encoding='utf-8') as output_file:
    relevant_files = []
    non_relevant_files = []
    previous_topicid = ""
    for line in input_file:
        [topicid, _, docid, rel] = line.strip().split()
        if topicid not in y_train:
           continue

        if topicid != previous_topicid:
            previous_topicid = topicid
            relevant_files = []
            non_relevant_files = []

        if rel == "1" or rel == "2":
            if len(non_relevant_files) > 0:
                negative_passage = random.choice(non_relevant_files)
                non_relevant_files.remove(negative_passage)
                positive_passage = docid

                write_line(topicid, positive_passage.replace('\t', ' ').replace('\n', ' '), negative_passage.replace('\t', ' ').replace('\n', ' '))
            else:
                relevant_files.append(docid)
        elif rel == "0":
            if len(relevant_files) > 0:
                positive_passage = random.choice(relevant_files)
                relevant_files.remove(positive_passage)
                negative_passage = docid

                write_line(topicid, positive_passage.replace('\t', ' ').replace('\n', ' '), negative_passage.replace('\t', ' ').replace('\n', ' '))
            else:
                non_relevant_files.append(docid)