import getpass
import os

# Global variables
split_file_count = 20
input_file = '/ceph/csedu-scratch/course/I00041_informationretrieval/eantonova/data/short_abstracts_en.ttl'
#input_file = '/home/'+ getpass.getuser() + '/data_sets/short_abstracts_en.ttl'
preprocess_directory = 'preprocessed_files'
parse_directory = 'plain_text_documents'
dbpedia_collection_path = 'dbpedia_index'
queries_file = 'data_set/queries-v2_stopped.txt'
qrels_file = 'data_set/qrels-v2.txt'
triples_file = 'triples.tsv'
output_monoT5_model_path = 'fine-tuned_monoT5_model'

os.makedirs(preprocess_directory, exist_ok=True)

# WARNING: Update variable to desired behaviour
test_small_batch = False