from global_var_lib import *

import os
import json
import pickle
import numpy as np
import pandas as pd
from tqdm.auto import tqdm
import torch
from torch.utils.data import Dataset
import jsonlines
import argparse
from pygaggle.pygaggle.rerank.transformer import MonoT5
from transformers import (
    AutoTokenizer,
    AutoConfig,
    AutoModelForSequenceClassification,
    AutoModelForSeq2SeqLM,
    Seq2SeqTrainer,
    Seq2SeqTrainingArguments,
    DataCollatorForSeq2Seq,
    TrainerCallback,
)

class MonoT5Dataset(Dataset):
    def __init__(self, data):
        self.data = data

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        sample = self.data[idx]
        text = f'Query: {sample[0]} Document: {sample[1]} Relevant:'
        return {
          'text': text,
          'labels': sample[2],
        }

# Emmas paper: https://arxiv.org/pdf/2205.00820.pdf

base_model = 'castorini/monot5-base-msmarco' # Emma used 'castorini/monot5-large-msmarco', but we lack the memory
triples_path = triples_file
output_model_path = output_monoT5_model_path
save_every_n_steps = 0
logging_steps = 100
per_device_train_batch_size = 4 # in Emmas code it is 8, but we lack the memory
per_device_eval_batch_size = 8 # in Emmas code it is 64, but we lack the memory
gradient_accumulation_steps = 16
learning_rate = 10e-6
epochs = 10

device = torch.device('cuda')
torch.manual_seed(123)

model = AutoModelForSeq2SeqLM.from_pretrained(base_model)
tokenizer = AutoTokenizer.from_pretrained('t5-base')

train_samples = []
with open(triples_path, 'r', encoding="utf-8") as fIn:
    for num, line in enumerate(fIn):
        if num > 6.4e5 * epochs:
            break
        query, positive, negative = line.split("\t")
        train_samples.append((query, positive, 'true'))
        train_samples.append((query, negative, 'false'))

def smart_batching_collate_text_only(batch):
    texts = [example['text'] for example in batch]
    tokenized = tokenizer(texts, padding=True, truncation='longest_first', return_tensors='pt', max_length=512)
    tokenized['labels'] = tokenizer([example['labels'] for example in batch], return_tensors='pt')['input_ids']

    for name in tokenized:
        tokenized[name] = tokenized[name].to(device)

    return tokenized

dataset_train = MonoT5Dataset(train_samples)

if save_every_n_steps:
    steps = save_every_n_steps
    strategy = 'steps'
else:
    steps = 1
    strategy = 'epoch'

train_args = Seq2SeqTrainingArguments(
    output_dir=output_model_path,
    do_train=True,
    save_strategy=strategy,
    save_steps =steps,
    logging_steps=logging_steps,
    per_device_train_batch_size=per_device_train_batch_size,
    per_device_eval_batch_size=per_device_eval_batch_size,
    gradient_accumulation_steps=gradient_accumulation_steps,
    learning_rate=learning_rate,
    # weight_decay=5e-5,
    num_train_epochs=1,
    warmup_steps=4000,
    seed=1,
    disable_tqdm=False,
    load_best_model_at_end=False,
    predict_with_generate=True,
    dataloader_pin_memory=False,
    remove_unused_columns=False,
)

trainer = Seq2SeqTrainer(
    model=model,
    args=train_args,
    train_dataset=dataset_train,
    tokenizer=tokenizer,
    data_collator=smart_batching_collate_text_only,
)

trainer.train()

trainer.save_model(output_model_path)
trainer.save_state()
