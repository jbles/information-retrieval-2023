import os

directory = 'pygaggle'
for subdir, dirs, files in os.walk(directory):
    for filename in files:
        if filename.endswith('.py'):
            # Read in the file
            with open(os.path.join(subdir, filename), 'r') as file:
              filedata = file.read()
            
            # Ensure pygaggle is replaced only once
            if 'from pygaggle.pygaggle.' in filedata:
              continue

            # Replace the target string
            filedata = filedata.replace('from pygaggle.', 'from pygaggle.pygaggle.')

            # Write the file out again
            with open(os.path.join(subdir, filename), 'w') as file:
              file.write(filedata)