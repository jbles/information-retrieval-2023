from global_var_lib import *
from sklearn.model_selection import train_test_split

def split_dataset():
    with open(queries_file, "r", encoding="utf-8") as f:
        data = f.read().split('\n')

    y = []
    topic_ids = []
    queries = []
    for d in data:
        if not d:
            continue  # Skip empty lines

        parts = d.split('\t')
        y_class = parts[0]
        query = parts[1]

        if y_class.startswith("INEX_LD"):
            y.append("INEX_LD")
        elif y_class.startswith("INEX_XER"):
            y.append("INEX_XER")
        elif y_class.startswith("QALD2_te"):
            y.append("QALD2_te")
        elif y_class.startswith("QALD2_tr"):
            y.append("QALD2_tr")
        elif y_class.startswith("SemSearch_ES"):
            y.append("SemSearch_ES")
        elif y_class.startswith("SemSearch_LS"):
            y.append("SemSearch_LS")
        elif y_class.startswith("TREC_Entity"):
            y.append("TREC_Entity")
        queries.append(query)
        topic_ids.append(y_class)
    return train_test_split(queries, topic_ids, test_size=0.33, random_state=42, shuffle=True, stratify=y)

