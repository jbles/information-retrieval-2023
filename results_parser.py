

def update_dict(dict, key, score):
    if key not in dict:
        # First value is sum, second value is count
        dict[key] = {"sum": 0.0, "count": 0.0}

    dict[key]["sum"] += float(score)
    dict[key]["count"] += 1.0

    return dict

def calculate_total(dict):
    total = 0.0
    count = 0.0
    for key, value in dict.items():
        subtotal = value["sum"] / value["count"]
        dict[key]["subtotal"] = subtotal

        total += subtotal
        count += 1.0
    dict["total"] = total / count
    
    return dict

def print_results(dict, title):
    print(title)
    for key, value in dict.items():
        if key == "total":
            print(key + ":\t" + str(value))
        else:
            print(key + ":\t" + str(value["subtotal"]))
    print()

def parse_results(input_file):
    with open(input_file, 'r') as file:
        data = file.readlines()
    
    k10_results = {}
    k100_results = {}
    for d in data:
        score = d.split(" ")[1]
        query = d.split(" ")[0]
        if query.startswith("INEX_LD"):
            if query.endswith("10"):
                k10_results = update_dict(k10_results, "INEX_LD", score)
            elif query.endswith("100"):
                k100_results = update_dict(k100_results, "INEX_LD", score)
        elif query.startswith("INEX_XER"):
            if query.endswith("10"):
                k10_results = update_dict(k10_results, "ListSearch", score)
            elif query.endswith("100"):
                k100_results = update_dict(k100_results, "ListSearch", score)
        elif query.startswith("QALD2_te"):
            if query.endswith("10"):
                k10_results = update_dict(k10_results, "QALD2", score)
            elif query.endswith("100"):
                k100_results = update_dict(k100_results, "QALD2", score)
        elif query.startswith("QALD2_tr"):
            if query.endswith("10"):
                k10_results = update_dict(k10_results, "QALD2", score)
            elif query.endswith("100"):
                k100_results = update_dict(k100_results, "QALD2", score)
        elif query.startswith("SemSearch_ES"):
            if query.endswith("10"):
                k10_results = update_dict(k10_results, "SemSearch_ES", score)
            elif query.endswith("100"):
                k100_results = update_dict(k100_results, "SemSearch_ES", score)
        elif query.startswith("SemSearch_LS"):
            if query.endswith("10"):
                k10_results = update_dict(k10_results, "ListSearch", score)
            elif query.endswith("100"):
                k100_results = update_dict(k100_results, "ListSearch", score)
        elif query.startswith("TREC_Entity"):
            if query.endswith("10"):
                k10_results = update_dict(k10_results, "ListSearch", score)
            elif query.endswith("100"):
                k100_results = update_dict(k100_results, "ListSearch", score)

    k10_results = calculate_total(k10_results)
    k100_results = calculate_total(k100_results)

    print_results(k10_results, "k10")
    print_results(k100_results, "k100")
    print("k=10:", k10_results, end="\n\n")
    print("k=100:", k100_results, end="\n\n")

print("Default:")
parse_results("results_NDCG_default")
print("Fine-tuned:")
parse_results("results_NDCG_pretrained")