#!/usr/bin/env bash
### notes
# this experiment is meant to try out training the default k-nn model locally

# location of repository and data
project_dir=. # assume sbatch is called from root project dir
cifar10_folder=$project_dir/data/cifar10

# hyperparameters
k=1

# execute train CLI
source "$project_dir"/venv/bin/activate
python "$project_dir"/run_knn.py \
  --data_folder "$cifar10_folder" \
  --k $k
