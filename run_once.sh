#!/bin/bash -e
#SBATCH --partition=csedu
#SBATCH --account=csedui00041
#SBATCH --gres=gpu:1
#SBATCH --mem=15G
#SBATCH --cpus-per-task=1
#SBATCH --time=6:00:00
#SBATCH --output=my-experiment-%j.out
#SBATCH --error=my-experiment-%j.err
#SBATCH --mail-type=FAIL

source /scratch/$USER/virtual_environments/information-retrieval-2023/bin/activate

git clone --recursive https://github.com/castorini/pygaggle.git

python preprocess.py

python pygaggle_update.py

python -m pyserini.index.lucene -collection JsonCollection \
    -generator DefaultLuceneDocumentGenerator -threads 1 \
    -input plain_text_documents -index dbpedia_index \
    -storePositions -storeDocvectors -storeRaw