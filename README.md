
# Server quickstart

I followed this tutorial to get setup the server: https://gitlab.science.ru.nl/das-dl/cluster-skeleton-python . So you can refer to this if you need further information.

So let's start setting up the server: 

1. inside your terminal do ```nano ~/.profile```
2. add the following line: 
```
# disable pip caching downloads
export PIP_NO_CACHE_DIR=off
```
3. Make sure to activate the change in your current shell by running source ```~/.profile```
after you've added the line. You can verify you set it correctly by running
echo ```$PIP_NO_CACHE_DIR``` and seeing off being printed.

4. go to /ceph/csedu-scratch/course/I00041_informationretrieval and create a new directory ```mkdir -p {$USERNAME}```
5.  ```chmod 700 {$USERNAME}```
6. cd to /ceph/csedu-scratch/course/I00041_informationretrieval/{$USERNAME}
7. git clone the repository on gitlab
8. ```cd information-retrieval-2023```, so now your you should be in /ceph/csedu-scratch/course/I00041_informationretrieval/{$USERNAME}/information-retrieval-2023
9. use the script prepare_cluster.sh to generate the symlinks for us: ```./scripts/prepare_cluster.sh```
10. Verify with ```ls -la``` that the expected symlinks have been created.
11. You can simply run ```./scripts/setup_virtual_environment.sh``` to create the
virtual environment. This will also install everything written in requirements.txt

check with pip list if all the requirements are installed. if not or if it failed installing because not enough space was available run the following: ```TMPDIR=/scratch/vphan pip install -r requirements.txt```

12. You can activate the virtual environment by typing ```source venv/bin/activate```, the name of the env is probably 'information-retrieval-2023'

NOTE: If you want to add another dependency, I recommend adding it to the requirements.txt file and then running ```pip install -r requirements.txt```.
Ensure the virtual environments is activated while doing this!

13. If you're on the cluster, the virtual environment will have been put on the /scratch/ filesystem, which is a local disk to each node. To ensure the virtual environment is accessible on every node, you should run
```./scripts/sync_csedu.sh``` (bachelor/master students). You need to rerun the script everytime you add a dependency to requirements.txt. Make sure you are on (cn84) when running this command. probably takes a bit of because it submits a run on every node/ wait in queue be able to run on the nodes.

NOTE: Furthermore, never manually add a dependency with e.g. pip install x, but instead add to requirements.txt and rerun the sync script. Manually adding it with pip will only update the virtual environment on cn84, while the virtual environments should also be updated on each node.

NOTE: you can also manually sync it if the script doesn't work with sync_venv_cnxx.sh script in the root.

NOTE: if your pip install gives these warnings: 'this is taking longer than usual. You might need to provide the dependency resolver with stricter constraints to reduce runtime.', try to upgrade your pip with ```pip install --upgrade pip```

14. Now we need to set our Java home for pyserini to work. run the following ```export JAVA_HOME=/lib/jvm/java-11-openjdk-amd64```
15. run ```echo $JAVA_HOME``` and check if it prints /lib/jvm/java-11-openjdk-amd64

# Running scripts
Now time to start training.

1. Run ```sbatch run_once.sh``` script once!. If any errors occur then delete the created pygaggle dir first before running it again.

2.  Run ```sbatch run.sh```


<!-- Order of files to execute:
Julians stuff:


* First make sure that commands of `requirements.txt` are executed or installed (somehow).
* Make sure `global_var_lib.py` is in the directory, you do not need to run it but it has to be there.
* Run `preprocess.py` to preprocess the data, maybe update the filepaths in `global_var_lib.py` if necessary.
* Maybe run `pygaggle_update.py` to make the cloned directory work, important to note. If this script makes it not work then reclone the git repository of pygaggle. This script is not revertable!
* Run `fine-tune_monoT5.py` to fine-tune monoT5 on the DBpedia dataset, it will create a model directory that will be used in the following file.
* Run `dbpedia_indexer.sh` (or run it in the terminal) to index the dbpedia collection.
* Run `rank.py` to rank the queries from DBpedia, either add no command line argument to rank with normal monoT5 or add `pretrained` as command line argument to train with the pretrained model. -->
