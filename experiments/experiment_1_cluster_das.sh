#!/usr/bin/env bash
#SBATCH --partition=das
#SBATCH --account=das
#SBATCH --qos=das-normal
#SBATCH --account=das
#SBATCH --mem=10G
#SBATCH --cpus-per-task=6
#SBATCH --time=6:00:00
#SBATCH --output=./logs/experiment1_%j.out
#SBATCH --error=./logs/experiment1_%j.err
#SBATCH --mail-type=BEGIN,END,FAIL

### notes
# this experiment is meant to try out training the default k-nn model on the cluster

# location of repository and data
project_dir=. # assume sbatch is called from root project dir
cifar10_folder=$project_dir/data/cifar10

# hyperparameters
k=1

# execute train CLI
source "$project_dir"/venv/bin/activate
python "$project_dir"/run_knn.py \
  --data_folder "$cifar10_folder" \
  --k $k