#!/usr/bin/env bash
#SBATCH --partition=csedu
#SBATCH --account=cseduproject
#SBATCH --qos=csedu-normal
#SBATCH --array=3,5,7,9,15
#SBATCH --mem=10G
#SBATCH --cpus-per-task=6
#SBATCH --time=6:00:00
#SBATCH --output=./logs/experiment2_%j_k%a.out
#SBATCH --error=./logs/experiment2_%j_k%a.err
#SBATCH --mail-type=BEGIN,END,FAIL

### notes
# this experiment is meant to try out different values of k

# location of repository and data
project_dir=. # assume sbatch is called from root project dir
cifar10_folder=$project_dir/data/cifar10

# hyperparameters
k=$SLURM_ARRAY_TASK_ID

# execute train CLI
source "$project_dir"/venv/bin/activate
python "$project_dir"/run_knn.py \
  --data_folder "$cifar10_folder" \
  --k $k