from setuptools import setup

setup(
   name='information-retrieval-2023',
   version='0.1',
   description='code for monot5 training for information-retrieval-2023',
   author='Mel Phan',
   author_email='mel.phan@ru.nl',
   packages=['server_files'],
)