#!/bin/bash -e
#SBATCH --partition=csedu
#SBATCH --account=csedui00041
#SBATCH --gres=gpu:1
#SBATCH --mem=15G
#SBATCH --cpus-per-task=1
#SBATCH --time=6:00:00
#SBATCH --output=my-experiment-%j.out
#SBATCH --error=my-experiment-%j.err
#SBATCH --mail-user=mel.phan@ru.nl
#SBATCH --mail-type=BEGIN,END,FAIL
#SBATCH --nodelist=cn78

mkdir -p /scratch/"$USER" 
rsync cn84:/scratch/"$USER"/virtual_environments/ /scratch/"$USER"/virtual_environments/ -ah --delete 
echo completed syncing to node
