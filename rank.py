from global_var_lib import *
from split_dataset import *

import math
import sys

from pyserini.search.lucene import LuceneSearcher

from pygaggle.pygaggle.rerank.base import Query, Text
from pygaggle.pygaggle.rerank.transformer import MonoT5
from pygaggle.pygaggle.rerank.base import hits_to_texts
from transformers import T5ForConditionalGeneration

from sklearn.metrics import ndcg_score

def DCG(query_relevancy_labels, k):
    # Use log with base 2
    # =======Your code=======
    sum = 0
    for i in range(min(k, len(query_relevancy_labels))):
      sum += query_relevancy_labels[i] / (math.log2(1 + (i + 1))) # Change i to i + 1 because i starts at 0

    return sum
    # =======================

def NDCG(query_relevancy_labels, k):
    # =======Your code=======
    # Calculate IDCG
    sorted_query = sorted(query_relevancy_labels, reverse=True)
    idcg = DCG(sorted_query, k)
    # Calculate nDCG
    if idcg == 0:
      return 0
    return DCG(query_relevancy_labels, k) / idcg
    # =======================

def rank(model_to_use):
    if model_to_use == "pretrained":
        print("Pretrained")
        model = T5ForConditionalGeneration.from_pretrained(output_monoT5_model_path)
        print("Model init")
        reranker = MonoT5(model=model)
        print("Reranker init")
    else:
        reranker = MonoT5()

    print("Loading queries...")

    _, _, _, y_test = split_dataset()
    # Load the queries from the file.
    queries = {}
    with open(queries_file, 'r') as file:
        for line in file:
            query_id, query_text = line.strip().split('\t')
            if query_id not in y_test:
                continue
            queries[query_id] = query_text

    qrel = {}
    with open(qrels_file, "r", encoding='utf-8') as input_file:
        for line in input_file:
            [topicid, _, docid, rel] = line.strip().split()
            if topicid not in y_test:
                continue
            qrel[str(topicid + "_" + docid)] = rel

    print("Initialize searcher")
    # Initialize the Pyserini Searcher for BM25.
    searcher = LuceneSearcher(dbpedia_collection_path)

    print("Start searcher")
    total_NDCG_10 = []
    total_NDCG_100 = []
    for query_id, query_text in queries.items():
        query = Query(query_text)

        # Search for relevant documents using the BM25 model.
        hits = searcher.search(query_text, 100)

        if len(hits) > 0:
            texts = hits_to_texts(hits)

        reranked = reranker.rerank(query, texts)
        #print(query_text)

        k_10 = 10
        k_100 = 100
        relevancy_10 = []
        relevancy_100 = []
        for i in range(0, min(len(hits), k_100)):
            query_doc = str(query_id + "_" + reranked[i].metadata["docid"])
            if query_doc in qrel:
                relevancy_100.append(int(qrel[query_doc]))
                if i < k_10:
                    relevancy_10.append(int(qrel[query_doc]))
            else:
                relevancy_100.append(0)
                if i < k_10:
                    relevancy_10.append(0)

        #sorted_query = sorted(relevancy_100, reverse=True)
        current_NDCG_10 = NDCG(relevancy_10, k_10)
        #current_NDCG = NDCG(relevancy_10, k_10)
        total_NDCG_10.append((query_id + "_10", current_NDCG_10))

        current_NDCG_100 = NDCG(relevancy_100, k_100)
        #current_NDCG_100 = ndcg_score([sorted_query], [relevancy_100], k=100)
        total_NDCG_100.append((query_id + "_100", current_NDCG_100))
    
    print("k=10:", total_NDCG_10)
    print("k=100:", total_NDCG_100)

    with open(("results_NDCG_" + model_to_use), "w", encoding='utf-8') as output_NDCG_file:
        output_NDCG_file.write('\n'.join(map(lambda x: ' '.join(map(str, x)), total_NDCG_10)))
        output_NDCG_file.write('\n')
        output_NDCG_file.write('\n'.join(map(lambda x: ' '.join(map(str, x)), total_NDCG_100)))

if __name__ == "__main__":
   model_to_use = "default"
   print(str(sys.argv))
   if len(sys.argv) > 1:
      print("Here!")
      model_to_use = "pretrained"
   rank(model_to_use)